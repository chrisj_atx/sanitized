#!/usr/bin/env python3

def getpod():
    import os
    pod = os.environ['PODNAME']
    node = os.environ['NODENAME']
    return [pod, node]
