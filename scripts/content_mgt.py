def Content():
    TOPIC_DICT = {"RPI":[["Raspberry Pi Kiosk", "/pi-kiosk/",
                          " Use the Raspberry Pi and Python to create a cheap kiosk.", "kiosk.jpg",
                          " The Raspberry Pi can be used as capable and inexpensive kiosk for personal or even business use.  This project will go over setup of the Raspberry Pi and how to create the Python code to drive a browser."],
			 ["Pi Radio", "/pi-radio/", "The Raspberry Pi makes a great internet radio!", "pi-radio.jpg", "I turned an old broken 1930s TrueTone radio into an Internet radio with a custom front-end written in Python3." ],
                         ["Arcade 1up MAME cabinent","/raspberry_mame/",
                          " A fun way to relive the days of the arcade machine!", "cabinet_mvc.jpg",
                          " When I was growing up arcades were everywhere, now they are rare. By modifying an Arcade 1up machine you can play all your old favorites!"]],
                  "Random":[["Cellular Drive Test Mapping", "/drive-test-map/",
                              " Coverage maps and handover analysis on a 0 dollar budget.", "phone.png",
                              "You can create coverage maps and create maps to do handover analysis with free tools."]],
                  "Cloud":[["Kubenetes Cluster Tutorial", "/kubernetes-cluster/",
                             "Start learning Kubernetes by deploying your own cluster.", "Kubernetes.png",
                             "Begin learning about Kubernetes by creating a two node Kubenetes cluster and using Calico for the container network!"]]}
    return TOPIC_DICT
