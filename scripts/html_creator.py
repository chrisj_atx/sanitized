from content_mgt import Content
import os


TOPIC_DICT = Content()


# ORDER OF %S: "Basics" , "Basics", "Basics, "Basics"

HTML_TEMPLATE = """
{% extends "header.html" %}
{% block body %}
<div class="gcont">
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class=""><a href="/">Home</a></li>
    <li role="presentation" class=""><a href="/projects/">Projects</a></li>
    <li role="presentation" class=""><a href="/about/">About</a></li>
    <li role="presentation" class="active"><a href="/pagename/">{{curTitle}}</a></li>
  </ul>
  <div class="sectiontitle" style="max-width: 800px; margin: 0 auto">
  <h2>{{curTitle}}</h2>
  <hr style="max-width: 800px; margin: 0 auto">
  <br />
  <img style="max-width: 65%" src="{{ url_for('static', filename='images/image.ext') }}">
  <p><br />Introduction</p>

  <hr />
  <ul class="blogul">
    <li><a href="#One" data-toggle="collapse">One</a>
      <span>
        <div id="One" class="collapse">
          <br />
          <p>Content</p><br />
          <hr />
        </div>
      </span>
    </li>
    <li><a href="#Two" data-toggle="collapse">Two</a>
      <span>
        <div id="Two" class="collapse">
          <br />
          <div class="code">
            <pre class="prettyprint">Code Block Example
              asdf
            </pre>
          </div>
          <br />
          <p>Content</p><br />
          <br />
          <hr />
        </div>
      </span>
    </li>
  </ul>
  <hr>
  <div class="col l6">
    <br />
    <center><button onclick="goBack()" class="btn"><span>Back</span></button><br /></center>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script>
    $('#myTab a').click(function(e) {
      e.preventDefault()
      $(this).tab('show')
    })
  </script>
  </div>
</div>
{% endblock %}
"""

for each_topic in TOPIC_DICT:
    print(each_topic)
    try:
        os.makedirs(each_topic)
    except FileExistsError:
        pass

    for eachele in TOPIC_DICT[each_topic]:
        try:

            filename = (eachele[1]+'.html').replace("/","")
            print(filename)
            savePath = each_topic+'/'+filename

            saveData = (HTML_TEMPLATE.replace("%s",each_topic))
            imgName = eachele[3]
            saveData = (HTML_TEMPLATE.replace("imgName", imgName))

            template_save = open(savePath,"w")
            template_save.write(saveData)
            template_save.close()
        except Exception as e:
            print(str(e))
