#!/var/www/FlaskApp/FlaskApp/venv/bin/python3
from flask import Flask, render_template, request, send_from_directory
from content_mgt import Content

TOPIC_DICT = Content()

app = Flask(__name__)

@app.route('/robots.txt')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])

@app.route('/sitemap.xml')
def static_from_root_map():
    return send_from_directory(app.static_folder, request.path[1:])

@app.route('/BingSiteAuth.xml')
def static_from_root_bing():
    return send_from_directory(app.static_folder, request.path[1:])


@app.route('/')
def homepage():
    return render_template("main.html")

@app.route('/projects/')
def projects():
    return render_template("projects.html", TOPIC_DICT = TOPIC_DICT)
    #return("Projects")

@app.route('/about/')
def about():
    return render_template("about.html")

@app.route(TOPIC_DICT["RPI"][0][1])
def Raspberry_Pi_Kiosk():
    return render_template("RPI/pi-kiosk.html", curLink = TOPIC_DICT["RPI"][0][1], curTitle=TOPIC_DICT["RPI"][0][0])

@app.route(TOPIC_DICT["RPI"][1][1])
def Raspberry_Pi_Radio():
    return render_template("RPI/pi-radio.html", curLink = TOPIC_DICT["RPI"][1][1], curTitle=TOPIC_DICT["RPI"][1][0])


@app.route(TOPIC_DICT["RPI"][2][1])
def Bartop_MAME_cabinent():
    return render_template("RPI/bartop.html", curLink = TOPIC_DICT["RPI"][2][1], curTitle=TOPIC_DICT["RPI"][2][0])




@app.route(TOPIC_DICT["Telecom"][0][1])
def Cellular_Drive_Test_Mapping():
    return render_template("Telecom/drive_test_map.html", curLink = TOPIC_DICT["Telecom"][0][1], curTitle=TOPIC_DICT["Telecom"][0][0])





if __name__ == "__main__":
    app.run(host='0.0.0.0')
